from django.test import TestCase
from django.test import Client
from parliament.models import Division, Debate
import datetime


class HomeTest(TestCase):
    def test_divisions_are_present(self):
        """
        tests that divisions are returned 
        """
        #add a some divisions
        for i in range(1,10):
            #import pdb; pdb.set_trace()
            deb = Debate(title='debate '+str(i))
            deb.save()
            division = Division(date = datetime.date(1969,1,i), debate=deb)
            division.save()
        client = Client()
        response = client.get('/')
        self.assertEquals(200,response.status_code)
        self.assertIsNotNone(response.context['divisions'])
        divisions = response.context['divisions']
        self.assertTrue(len(divisions)>0)
        for division in divisions :
            self.assertIsInstance(division, Division)
            self.assertTrue(len(division.ayes)>0)
            self.assertTrue(len(division.noes)>0)
