# Create your views here.

from django.http import HttpResponseNotAllowed 
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from parliament.models import Division


def home(request):
    if(request.method == 'GET'):
        #get the last 5 divisions
        divisions = Division.objects.order_by("-date")[:10]
        return render_to_response('home.html',{'divisions' : divisions},context_instance=RequestContext(request));

    return HttpResponseNotAllowed(['GET'])

