from django.conf.urls import patterns, url, include
from django.contrib import admin
from users import views as users_views
from main import views as main_views
from parliament import views as parliament_views

admin.autodiscover()

loginUrl = '/login/'
logoutUrl = '/logout/'
searchUrl = '/parliament/politicians'

urlpatterns = patterns('',

    url(r'^$', main_views.home, name='main'),
    url(r'^login/', users_views.login, name='login'),
    url(r'^register/', users_views.register, name='register'),
    url(r'^authorization/', users_views.manage_authorization, name='manage_authorization'),
    url(r'^parliament/politicians/$', parliament_views.search_politicians, name='politicians'),
    url(r'^admin/', include(admin.site.urls)),
)
