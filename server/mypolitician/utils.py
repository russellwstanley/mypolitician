from parliament.models import Politician, Division, Debate
import random
import datetime

def create_divisions(count):
    politicians = Politician.objects.all()
    options = { 
    0 : novote,
    1 : vote_aye,
    2 : tell_aye,
    3 : vote_no,
    4 : tell_no }
    for i in range(1, count+1):
        deb = Debate(title='debate '+str(i))
        deb.save()
        division = Division(date=datetime.date(1969,1,i),debate=deb)
        division.save()
        for politician in politicians:
            choice = random.randint(0,4)
            options[choice](division,politician)
        division.save()


def novote(division,politician):
    pass

def vote_aye(division,politician):
    division.ayes.add(politician)
    

def tell_aye(division,politician):
    division.tellers_ayes.add(politician)

def vote_no(division,politician):
    division.noes.add(politician)

def tell_no(division,politician):
    division.tellers_noes.add(politician)
