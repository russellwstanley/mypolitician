from django.http import HttpResponse
from django.core.serializers import json, serialize
from django.db.models.query import QuerySet
from django.http import HttpResponse
from django.utils import simplejson
from haystack.query import SearchQuerySet

HTTP_STATUS_CODE_BAD_PARAMETERS=422
#TODO have a look at what codes we should be returning
HTTP_STATUS_CODE_BAD_CREDENTIALS=403


class HttpResponseBadParameters(HttpResponse):
    status_code=HTTP_STATUS_CODE_BAD_PARAMETERS

class HttpResponseInvalidCredentials(HttpResponse):
    status_code=HTTP_STATUS_CODE_BAD_CREDENTIALS

class JsonResponse(HttpResponse):
    def __init__(self, object):
        if isinstance(object, SearchQuerySet):
            #import pdb; pdb.set_trace()
            content = serialize("json", [x.object for x in object])
        elif isinstance(object, QuerySet):
            content = serialize('json', object)
        else:
            content = simplejson.dumps(
                object, indent=2, cls=json.DjangoJSONEncoder,
                ensure_ascii=False)
        super(JsonResponse, self).__init__(
            content, content_type='application/json')
