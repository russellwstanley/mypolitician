from django.http import HttpResponse, HttpResponseNotAllowed ,HttpResponseRedirect
import django.contrib.auth as auth
from django import forms
from common.http_responses import HttpResponseBadParameters, HttpResponseInvalidCredentials
from django.shortcuts import render_to_response
from django.template import RequestContext

class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField()

#TODO duplication in the 2 login methods
def login(request):
    #import pdb; pdb.set_trace();

    #return the login page
    if(request.method == 'GET'):
        return render_to_response('login.html',{},context_instance=RequestContext(request));
    if(request.method == 'POST'):
        userInput = LoginForm(request.POST)
        if(not userInput.is_valid()):
            return HttpResponseBadParameters()
        username = userInput.cleaned_data['username']
        password = userInput.cleaned_data['password']
        user = auth.authenticate(username=username, password=password)
        if user == None:
            return HttpResponseInvalidCredentials()
        auth.login(request,user)
        return HttpResponseRedirect('/home/')
    return HttpResponseNotAllowed(['GET','POST'])


def register(request):
    #return the registration page
    pass



def manage_authorization(request):
    #import pdb; pdb.set_trace();
    if(request.method == 'GET'):
        userInput = LoginForm(request.GET)
        if(not userInput.is_valid()):
            return HttpResponseBadParameters()
        username = userInput.cleaned_data['username']
        password = userInput.cleaned_data['password']
        user = auth.authenticate(username=username, password=password)
        if user == None:
            return HttpResponseInvalidCredentials()
        auth.login(request,user)
        return HttpResponse()

    if(request.method == 'DELETE'):
        if request.user.is_authenticated():
            auth.logout(request)
            return HttpResponse()

    return HttpResponseNotAllowed(['GET','DELETE'])






