from django.test.client import Client
from django.test import TestCase
from django.contrib.auth.models import User
import common.http_responses 
from django.contrib.auth import SESSION_KEY

class UsersTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.loginUrl = '/authorization/'
        self.logoutUrl = '/authorization/'
        self.testusername = 'testuser'
        self.testpass = 'testpas'
        User.objects.create_user(username=self.testusername,password=self.testpass)

    def testLogin_missingCredentials(self):
        response = self.client.get(self.loginUrl)
        self.assertEquals(common.http_responses.HTTP_STATUS_CODE_BAD_PARAMETERS,response.status_code)

    def testLogin_badCredentials(self):
        response = self.client.get(self.loginUrl,{'username':'bad','password':'credentials'})
        self.assertEquals(common.http_responses.HTTP_STATUS_CODE_BAD_CREDENTIALS,response.status_code)

    def testLoginLogout(self):
        response = self.client.get(self.loginUrl,{'username':self.testusername,'password':self.testpass})
        self.assertEquals(200,response.status_code)
        response = self.client.delete(self.logoutUrl,{'username':self.testusername,'password':self.testpass})
        self.assertEquals(200,response.status_code)

"""this does not seem to work at the moment
    def userIsAuthenticated(self):
        return SESSION_KEY in self.client.session"""
