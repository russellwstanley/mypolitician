from django.db import models
from django.test import TestCase
from django import forms
from django.db.models.signals import pre_save
from django.dispatch import receiver
import datetime

class Debate(models.Model):
    title = models.TextField()

class Politician(models.Model):

    first_name = models.CharField(max_length=60)
    last_name = models.CharField(max_length=60)
    constituency = models.CharField(max_length=128, unique=True)
    party = models.CharField(max_length=60)

class PoliticianForm(forms.ModelForm):
    class Meta:
        model = Politician

class Division(models.Model):

    date = models.DateField(null = False)
    ayes = models.ManyToManyField(Politician,related_name='division_ayes')
    noes = models.ManyToManyField(Politician,related_name='division_noes')
    tellers_ayes = models.ManyToManyField(Politician,related_name='division_tellers_ayes')
    tellers_noes = models.ManyToManyField(Politician,related_name='division_tellers_noes')

    debate = models.ForeignKey(Debate)

    


#@receiver(pre_save, sender=Division)
#def add_debate_if_empty(sender, **kwargs):
#    if(sender.debate == None):
#        deb = Debate();
#        deb.save();
#        sender.debate = deb;


class Speech(models.Model):
    politician = models.ForeignKey(Politician)
    debate = models.ForeignKey(Debate)
    text = models.TextField()

class PoliticainTest(TestCase):
    def test_empty_values(self):
        p = Politician()
        p.save()

class DivisionTest(TestCase):

    def test_vote_aye(self):
        deb = Debate(title='foo')
        deb.save()
        d = Division(debate=deb, date=datetime.date.today())
        d.save()
        self.assertEquals(0,len(d.ayes.all()))
        p = Politician()
        p.save()
        d.ayes.add(p)
        self.assertEqual(p,d.ayes.all()[0])

    

