from django.test import TestCase
from parliament.models import Politician
from parliament.models import Division
from parliament.scrapers import scrape_members as sm
import datetime


class ScrapersTest(TestCase):

    def test_scrape(self):
        politicians = Politician.objects.all()
        self.assertEquals(0,len(politicians))
        sm.scrape()
        politicians = Politician.objects.all()
        for politician in politicians:
#            print "first :"+politician.first_name+"#"
#            print "last :"+politician.last_name+"#"
#            print "constituency :"+politician.constituency+"#"
#            print "party :"+politician.party+"#"

            self.assertNotEqual("",politician.first_name)
            self.assertNotEqual("",politician.last_name)
            self.assertNotEqual("",politician.party)
            self.assertTrue(self.brackets_are_balanced(politician.party))
            self.assertNotEqual("",politician.constituency)

    def test_scrape_divisions(self):
        #d = datetime.date(2012,10,15)
        #results = sm.scrape_divisions(201213,d);
        #print results;
        pass;
        
    def brackets_are_balanced(self,string):
        return string.count(')') == string.count('(')

