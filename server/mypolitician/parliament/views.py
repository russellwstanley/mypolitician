from django.http import HttpResponseNotAllowed, HttpResponse
from common.http_responses import HttpResponseBadParameters, JsonResponse
from django import forms
from parliament.models import Politician
from haystack.inputs import AutoQuery
from haystack.query import SearchQuerySet
from django.shortcuts import render_to_response
from django.core import serializers

class SearchForm(forms.Form):
    search = forms.CharField(required=False)
    format = forms.CharField(required=False)

def getCorrectResponse(result,format):
    if(format=='json'  or not format):
        return JsonResponse(result)
    if(format=='html'):
        if isinstance(result, SearchQuerySet):
            return render_to_response('search_results.html', {'politicians': [x.object for x in result] } ,
        mimetype="application/xhtml+xml")
        #import pdb; pdb.set_trace();
        return render_to_response('search_results.html', {'politicians': result } ,
        mimetype="application/xhtml+xml")
    return HttpResponseBadParameters()

def search_politicians(request):
    if(request.method !='GET'):
        return HttpResponseNotAllowed(['GET'])
    userInput = SearchForm(request.GET)
    if(not userInput.is_valid()): 
        return HttpResponseBadParameters()
    search = userInput.cleaned_data['search']
    format = userInput.cleaned_data['format']
    if(search):
        sqs = SearchQuerySet().autocomplete(text=search)
        return getCorrectResponse(sqs,format)
    return getCorrectResponse(Politician.objects.all(),format)
