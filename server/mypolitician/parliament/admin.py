from django.contrib import admin
from parliament.models import Politician
from parliament.models import Division
from parliament.models import Speech
from parliament.models import Debate

admin.site.register(Politician)
admin.site.register(Division)
admin.site.register(Speech)
admin.site.register(Debate)


