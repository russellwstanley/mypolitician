from bs4 import BeautifulSoup
import requests
from parliament.models import Politician, PoliticianForm
import re


def scrape():
    response = requests.get("http://www.parliament.uk/mps-lords-and-offices/mps/")
    if(response.status_code!=200):
        raise Exception("unexpected status code")
    soup = BeautifulSoup(response.text,'html5lib')
    member_list = soup.findAll('tr', id=re.compile('^ctl00_ctl00_FormContent_SiteSpecificPlaceholder_PageContent_rptMembers'))
    print str(len(member_list)) + " members found"
    for element in member_list:
        name_elements = element('a')
        if(len(name_elements)!=1):
            raise Exception("unexpected element found"+str(name_elements))
        name = name_elements[0].contents[0]
        first_name = name.split(',')[1].strip()
        last_name = name.split(',')[0].strip()
        party_and_constituency_pair = element('td')
        if(len(party_and_constituency_pair)!=2):
            raise Exception("unexpected element found")
        party = extract_from_parens(party_and_constituency_pair[0].contents[2]).strip()
        constituency = party_and_constituency_pair[1].contents[0].strip()
        if (Politician.objects.filter(constituency=constituency).count() != 0):
            print "Politician already exists for "+'constituency '
            continue #todo figure out a way to update
        form = PoliticianForm({'first_name':first_name,'last_name':last_name,'party':party,'constituency':constituency})
        if not form.is_valid():
             raise Exception("invalid data scraped from website")
        form.save()

#urls look like this
#http://www.publications.parliament.uk/pa/cm201213/cmhansrd/cm121015/debindx/121015-x.htm
#http://www.publications.parliament.uk/pa/cm201213/cmhansrd/cm201213/debindx/121015-x.htm

#http://www.publications.parliament.uk/pa/cm201213/cmhansrd/cm201213/debindx/20121020-x.htm
#http://www.publications.parliament.uk/pa/cm201213/cmhansrd/cm201213/debindx/20121012-x.htm

def scrape_divisions(session,date):
    #get last 2 digits from year
    datestring = str(date.year)[-2:] + str(date.month) + str(date.day)
    rooturl='http://www.publications.parliament.uk'
    url = rooturl+"/pa/cm"+str(session)+"/cmhansrd/cm"+datestring+"/debindx/"+datestring+'-x.htm'
    #print(url)
    response = requests.get(url)
    soup = BeautifulSoup(response.text,'html5lib')
    division_list = soup.findAll('b', text=re.compile('^Division No. \d{1,9}'))
    for element in division_list:
        title = element.text
        regex = re.compile("^Division No. \d{1,9}")
        match = regex.search(title)
        shorttitle=match.group(0)
        print shorttitle
        link = element.parent['href']
        response = requests.get(rooturl+link)
        soup = BeautifulSoup(response.text,'html5lib')
        division_marker = soup.find('b', text=re.compile(shorttitle))
        center = division_marker.parent
        btag = center.nextSibling
        ayes = []
        nays = []
        ayes_tellers = []
        nays_tellers = []
        import pdb; pdb.set_trace()
        ayes = center.find_next_sibling('p',text='AYES')
        member = ayes.nextSibling
        while(member != None and member.text != 'Tellers for the Ayes:') :
            print member.text
            member = member.nextSibling
        
    
        #print response.text

    #we are lookinf for a chunk that looks like this
    #<a href="/pa/cm201213/cmhansrd/cm121015/debtext/121015-0003.htm#12101531000794" shape="rect">
    #<b>Division No. 69 [Infrastructure (Financial Assistance) Bill] [15 Oct 2012]</b>
    #</a>

    

    return division_list






def extract_from_parens(string):
        return string[string.find("(")+1:string.rfind(")")]
        
  

