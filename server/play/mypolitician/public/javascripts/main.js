$( document ).ready(function() {
  
    $( "#searchbox" ).on("keyup", function(event) {
        //event.preventDefault();
        //alert( "Handler for .click() called." );
        var target = $( event.target );
        var c = $('#searchbox-dropdown').attr('class');
        var text = target.val();
        if(text.length > 0){
            target.dropdown('toggle');
            $('#searchbox-dropdown').load(encodeURI("../parliament/members"));
        }
        else{
            $('.open').removeClass('open');
        }
    });
});