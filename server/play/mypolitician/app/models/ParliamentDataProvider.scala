package models

import java.util.Date

case class Member(firstName:String,lastName:String,constituency:String,party:String)

case class Debate(title:String)

case class Division(date : Date, debate: Debate,ayes:List[Member], noes:List[Member], tellersAyes:List[Member],tellersNoes:List[Member])

/**
 * Created by russell on 08/06/14.
 */
class DummyDataProvider {
  def getMembers() : List[Member] = members


  val members = List(
    Member("Bob","mcFooble","Liverpool","Conservative"),
    Member("Sam","Digbert","Ipswich","Labor"),
    Member("Bob","Flemming","Bristol","Conservative"),
    Member("Hooters","McFooters","Brighton","Green"),
    Member("Tallulah","Feathers","Basingstoke","Conservative")
  )

  def getRecent(): List[Division] = List(
    Division(new Date(),Debate("NHS Funding"),List(members(0),members(1)),List(members(2)),List(members(3)),List(members(4))),
    Division(new Date(),Debate("Europe"),List(members(1),members(2)),List(members(3)),List(members(4)),List(members(0))),
    Division(new Date(),Debate("Crime"),List(members(4),members(0)),List(members(1)),List(members(2)),List(members(3)))

  )
}
