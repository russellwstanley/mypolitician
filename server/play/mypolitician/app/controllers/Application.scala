package controllers

import play.api._
import play.api.mvc._
import models.DummyDataProvider

object Application extends Controller {

  val parliamentDataProvider = new DummyDataProvider()

  def index = Action {

    Ok(views.html.main("Your new application is ready.",parliamentDataProvider.getRecent()))
  }
  
  def members = Action {
    Ok(views.html.searchResults(parliamentDataProvider.getMembers()))
  }

}