package co.uk.mypolitician;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;

/**
 * @author russell@radioactive.sg
 * @copyright RadioActive PTE Ltd, 2013
 */

public class AuthorizationHelper {

    public enum AuthorizationStatus {
        SUCCESS, FAILURE, PENDING
    }

    public interface AuthorizationCallback {

        public void onAuthorizationStatusChange(AuthorizationHelper.AuthorizationStatus status);

    }

    private static final int URL_LOADER = 1; //TODO how do we handle multiple requests eh??
    private Activity activity;

    public AuthorizationHelper(Activity activity) {
        this.activity = activity;
    }

    public Activity getActivity() {
        return activity;
    }

    public void attemptAuthorization(final String username, final String password, final AuthorizationCallback callback) {


        activity.getLoaderManager().restartLoader(URL_LOADER, null, new LoaderManager.LoaderCallbacks<Cursor>() {

            @Override
            public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
                Log.d(MyPolitician.LOG_TAG, "creating loader");
                String where = DatabaseContract.Authorization.COLUMN_NAME_USERNAME + "=?";
                String[] whereArgs = new String[]{username};
                String[] columns = {DatabaseContract.Authorization.COLUMN_NAME_IS_AUTHENTICATED};
                return new CursorLoader(getActivity(), MyPoliticianContentProvider.LOCAL_AUTH_URI, columns, where, whereArgs, null);
            }

            @Override
            public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
                callback.onAuthorizationStatusChange(getAuthStatus(cursor));
            }

            @Override
            public void onLoaderReset(Loader<Cursor> arg0) {
                //TODO what to do here
            }

        });

        Intent service = new Intent(activity, MyPoliticianRemoteAuthService.class);
        service.putExtra(MyPoliticianRemoteAuthService.USERNAME_KEY, username);
        service.putExtra(MyPoliticianRemoteAuthService.PASSWORD_KEY, password);
        activity.startService(service);

    }

    private AuthorizationStatus getAuthStatus(Cursor cursor) {

        if (cursor == null || cursor.getCount() == 0) {
            return AuthorizationStatus.PENDING;
        }
        Log.d(MyPolitician.LOG_TAG,cursor.getCount()+" items returned");
        cursor.moveToFirst();
        int authColumn = cursor.getColumnIndexOrThrow(DatabaseContract.Authorization.COLUMN_NAME_IS_AUTHENTICATED);
        int authStatus = cursor.getInt(authColumn);
        if (authStatus == DatabaseContract.Authorization.AUTHORIZATION_FAILURE) {
            return AuthorizationStatus.FAILURE;
        }
        if (authStatus == DatabaseContract.Authorization.AUTHORIZATION_SUCCESS) {
            return AuthorizationStatus.SUCCESS;
        }
        return AuthorizationStatus.PENDING;
    }
}
