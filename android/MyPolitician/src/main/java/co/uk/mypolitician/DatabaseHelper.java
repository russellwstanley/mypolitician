package co.uk.mypolitician;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String SQL_CREATE_ENTRIES = "CREATE TABLE " + DatabaseContract.Authorization.TABLE_NAME + " (" + DatabaseContract.Authorization._ID + " INTEGER PRIMARY KEY NOT NULL, "
            + DatabaseContract.Authorization.COLUMN_NAME_COOKIE + " TEXT , " + DatabaseContract.Authorization.COLUMN_NAME_USERNAME + " STRING UNIQUE, "
            + DatabaseContract.Authorization.COLUMN_NAME_PASSWORD + " STRING , "+DatabaseContract.Authorization.COLUMN_NAME_CSRF_TOKEN+" STRING , "+DatabaseContract.Authorization.COLUMN_NAME_IS_AUTHENTICATED+" INTEGER )";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + DatabaseContract.Authorization.TABLE_NAME;

    public static final int DATABASE_VERSION = 4;
    public static final String DATABASE_NAME = "mypolitician.db";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(SQL_CREATE_ENTRIES);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
        // This database is only a cache for online data, so its upgrade policy
        // is
        // to simply to discard the data and start over
        Log.d(MyPolitician.LOG_TAG, "upgrading database...");
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);

    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

}
