package co.uk.mypolitician;

import android.provider.BaseColumns;

public abstract class DatabaseContract {

    //global definitions go here

    public static final class Authorization implements BaseColumns {

        public static final String TABLE_NAME = "auth";
        public static final String COLUMN_NAME_USERNAME = "username";
        public static final String COLUMN_NAME_PASSWORD = "password";
        public static final String COLUMN_NAME_COOKIE = "cookie";
        public static final String COLUMN_NAME_CSRF_TOKEN = "csrftoken";
        public static final String COLUMN_NAME_IS_AUTHENTICATED = "authenticated";
        public static final int AUTHORIZATION_SUCCESS = 1;
        public static final int AUTHORIZATION_FAILURE = -1;
        public static final int AUTHORIZATION_UNKNOWN = 0;
    }

}
