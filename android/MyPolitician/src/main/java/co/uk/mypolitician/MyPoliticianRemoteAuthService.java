package co.uk.mypolitician;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.widget.ListAdapter;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MyPoliticianRemoteAuthService extends IntentService {

    public static final String WHERE_ARGS_KEY = "whereArgs";
    public static final String WHERE_KEY = "where";

    public static final String GET_ACTION = "GET";
    public static final String AUTH_CATEGORY = "AUTH";
    public static final String USERNAME_KEY = "username";
    public static final String PASSWORD_KEY = "password";

    public MyPoliticianRemoteAuthService() {
        super("MyPoliticianRemoteAuthService");
        CookieManager cookieManager = new CookieManager();
        CookieHandler.setDefault(cookieManager);

    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String username = intent.getStringExtra(USERNAME_KEY);
        String password = intent.getStringExtra(PASSWORD_KEY);
        String cookie = null;
        try {
            HttpResponse loginResponse = doRemoteLogin(username, password, "10.0.2.2", 8000);
            if (loginResponse.getStatusLine().getStatusCode() == 200) {
                for (Header header : loginResponse.getHeaders("Set-Cookie")) {
                    String value = header.getValue();
                    if (value.startsWith("sessionid")) {
                        cookie = value;
                    }
                }
            }
        } catch (IOException e) {
            Log.e(MyPolitician.LOG_TAG, e.getMessage());
        }
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.Authorization.COLUMN_NAME_USERNAME, username);
        //create the record if it doesn't exist;
        String where = DatabaseContract.Authorization.COLUMN_NAME_USERNAME + "=?";
        String[] whereArgs = new String[]{username};
        Cursor record = getContentResolver().query(MyPoliticianContentProvider.LOCAL_AUTH_URI, null, where, whereArgs, null);
        //create record with that username if it doesn't exist
        if (record.getCount() == 0) {
            getContentResolver().insert(MyPoliticianContentProvider.LOCAL_AUTH_URI, values);
        }
        values.put(DatabaseContract.Authorization.COLUMN_NAME_PASSWORD, password);
        if(cookie!=null){
            values.put(DatabaseContract.Authorization.COLUMN_NAME_COOKIE,cookie);
            values.put(DatabaseContract.Authorization.COLUMN_NAME_IS_AUTHENTICATED,DatabaseContract.Authorization.AUTHORIZATION_SUCCESS);
        }
        else{
            values.put(DatabaseContract.Authorization.COLUMN_NAME_IS_AUTHENTICATED,DatabaseContract.Authorization.AUTHORIZATION_FAILURE);
        }
        getContentResolver().update(MyPoliticianContentProvider.LOCAL_AUTH_URI, values, where, whereArgs);
    }

    public HttpResponse doRemoteLogin(String username, String password, String serviceHost, int port) throws IOException {
        HttpClient client = new DefaultHttpClient();
        HttpUriRequest request = new HttpGet("http://" + serviceHost + ":" + port + "/authorization?username=" + username + "&password=" + password);
        HttpResponse response = client.execute(request);
        response.getAllHeaders();
        Log.e(MyPolitician.LOG_TAG, response.toString());
        return response;
    }

}
