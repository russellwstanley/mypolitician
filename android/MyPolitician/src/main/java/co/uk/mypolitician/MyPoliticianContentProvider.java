package co.uk.mypolitician;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

public class MyPoliticianContentProvider extends ContentProvider{

	private DatabaseHelper	        database;
	public static final String	    PROTOCOL	    = "content://";
	public static final String	    DATABASE_NAME	= "mypolitician.db";
	public static final String	    AUTHORITY	    = "co.uk.mypolitician";
	public static final String	    REMOTE	        = "remote";
	public static final String	    LOCAL	        = "local";

	public static final Uri	        LOCAL_AUTH_URI	= Uri.parse(PROTOCOL + AUTHORITY + "/" + LOCAL + "/auth");
	// Used for the UriMacher
	public static final int	        LOCAL_AUTH	    = 1;
	public static final int	        REMOTE_AUTH	    = 2;

	private static final UriMatcher	sURIMatcher	    = new UriMatcher(UriMatcher.NO_MATCH);

	static {
		sURIMatcher.addURI(AUTHORITY, LOCAL + "/auth", LOCAL_AUTH);
		sURIMatcher.addURI(AUTHORITY, REMOTE + "/auth", REMOTE_AUTH);
	}

	@Override
	public int delete(Uri arg0, String arg1, String[] arg2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean onCreate() {
		database = new DatabaseHelper(getContext());
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] columns, String where, String[] whereArgs, String sortOrder) {
		Log.d(MyPolitician.LOG_TAG, "query called");
		Log.d(MyPolitician.LOG_TAG, uri.toString());
		int match = sURIMatcher.match(uri);
		switch (match) {
			case LOCAL_AUTH:
				// return a query to the data set
				Cursor cursor = getAuthCursor(columns, where, whereArgs);
				cursor.setNotificationUri(getContext().getContentResolver(), uri);
				return cursor;
			default:
				throw new IllegalArgumentException("unsupported uri: " + uri);
		}
	}

	private Cursor getAuthCursor(String[] columns, String where, String[] whereArgs) {
		return database.getReadableDatabase().query(DatabaseContract.Authorization.TABLE_NAME, columns, where, whereArgs, null, null, null);
	}

	@Override
	public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
		Log.d(MyPolitician.LOG_TAG, "update called");
		Log.d(MyPolitician.LOG_TAG, uri.toString());
		int match = sURIMatcher.match(uri);
		SQLiteDatabase db = database.getWritableDatabase();
		switch (match) {
			case LOCAL_AUTH:
				// return a query to the data set
				// call the service to sync the data
				int id = db.update(DatabaseContract.Authorization.TABLE_NAME, values, where, whereArgs);
				getContext().getContentResolver().notifyChange(uri, null);
				return id;
			case REMOTE_AUTH:
				// TODO registering a new user?
			default:
				throw new IllegalArgumentException("unsupported uri: " + uri);
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Log.d(MyPolitician.LOG_TAG, "insert called");
		Log.d(MyPolitician.LOG_TAG, uri.toString());
		int match = sURIMatcher.match(uri);
		SQLiteDatabase db = database.getWritableDatabase();
		switch (match) {
			case LOCAL_AUTH:
				db.insert(DatabaseContract.Authorization.TABLE_NAME, null, values);
				getContext().getContentResolver().notifyChange(uri, null);
				return uri;
			default:
				throw new IllegalArgumentException("unsupported uri: " + uri);

		}
	}

	public int matchUri(Uri uri) {
		return sURIMatcher.match(uri);
	}

}
