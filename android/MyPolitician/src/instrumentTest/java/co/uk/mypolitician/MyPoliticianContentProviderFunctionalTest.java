package co.uk.mypolitician;

import android.database.sqlite.SQLiteDatabase;
import android.test.ProviderTestCase2;
import android.test.mock.MockContentResolver;

public class MyPoliticianContentProviderFunctionalTest extends ProviderTestCase2<MyPoliticianContentProvider>{

	private MockContentResolver	mMockResolver;

	private SQLiteDatabase	    mDb;

	public MyPoliticianContentProviderFunctionalTest() {
		super(MyPoliticianContentProvider.class, MyPoliticianContentProvider.AUTHORITY);
		// TODO Auto-generated constructor stub
	}

	/*
	 * Sets up the test environment before each test method. Creates a mock content resolver,
	 * gets the provider under test, and creates a new database for the provider.
	 */
	@Override
	protected void setUp() throws Exception {
		// Calls the base class implementation of this method.
		super.setUp();

		// Gets the resolver for this test.
		mMockResolver = getMockContentResolver();

	}

	/*
	 *  This method is called after each test method, to clean up the current fixture. Since
	 *  this sample test case runs in an isolated context, no cleanup is necessary.
	 */
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/*
	 * Tests the provider's publicly available URIs. If the URI is not one that the provider
	 * understands, the provider should throw an exception. It also tests the provider's getType()
	 * method for each URI, which should return the MIME type associated with the URI.
	 */
	public void testUriAndGetType() {
		// Tests the MIME type for the notes table URI.

	}

}
