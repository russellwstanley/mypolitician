package co.uk.mypolitician;

import android.content.Intent;
import android.test.InstrumentationTestCase;
import android.util.Log;

import org.apache.http.HttpResponse;

import java.io.IOException;
import java.net.URL;

/**
 * Created by russell on 8/16/13.
 */
public class MyPoliticianRemoteAuthServiceTest extends InstrumentationTestCase {

    public void testLogin() throws IOException {
        MyPoliticianRemoteAuthService service  =  new MyPoliticianRemoteAuthService();
        URL testAuthURL = new URL("http://10.0.2.2");
        HttpResponse response = service.doRemoteLogin("russell","1badcat","10.0.2.2",8000);
        Log.e(MyPolitician.LOG_TAG, response.toString());
        assertEquals(200,response.getStatusLine().getStatusCode());
    }

    public void testLogin_BadCredentials() throws IOException {
        MyPoliticianRemoteAuthService service  =  new MyPoliticianRemoteAuthService();
        URL testAuthURL = new URL("http://10.0.2.2");
        HttpResponse response = service.doRemoteLogin("bad","credentials","10.0.2.2",8000);
        assertEquals(403, response.getStatusLine().getStatusCode());
    }
}
